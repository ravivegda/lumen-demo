<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Geodata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geodata', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('officename');
            $table->unsignedInteger('pincode')->unique();
            $table->string('officeType')->nullable();
            $table->string('deliverystatus')->nullable();
            $table->string('divisionname')->nullable();
            $table->string('regionname')->nullable();
            $table->string('circlename')->nullable();
            $table->string('taluk')->nullable();
            $table->string('districtname')->nullable();
            $table->string('statename')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
