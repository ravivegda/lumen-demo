<?php
 
namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class ProcessCSV implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
 
    protected $podcast;
 
    /**
    * Create a new task instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
 
    /**
    * Run the task.
    *
    * @return void
    */
    public function handle()
    {
        $logs = array();
        if (($open = fopen("http://data.gov.in/sites/default/files/all_india_pin_code.csv", "r")) !== FALSE) {
            $i = 1;
            while (($data = fgetcsv($open, 1000, ",")) !== FALSE) {
                if($i>1)
                {   
                    try{                    
                        $geodata = new \App\Models\GeoData();
                        $geodata->officename = $data[0];
                        $geodata->pincode = $data[1];
                        $geodata->officeType = $data[2];
                        $geodata->deliverystatus = $data[3];
                        $geodata->divisionname = $data[4];
                        $geodata->regionname = $data[5];
                        $geodata->circlename = $data[6];
                        $geodata->taluk = $data[7];
                        $geodata->districtname = $data[8];
                        $geodata->statename = $data[9];
                        $geodata->save();
                    }
                    catch(\Exception $e)
                    {
                        $data['comments'] = "Pincode ".$data[1]." is already exists.";
                        $logs[] = $data;                        
                    }
                }
                $i++;
            }
            fclose($open);
        }

        if($logs)
        {
            $header[] = 'officename';
            $header[] = 'pincode';
            $header[] = 'officeType';
            $header[] = 'Deliverystatus';
            $header[] = 'divisionname';
            $header[] = 'regionname';
            $header[] = 'circlename';
            $header[] = 'Taluk';
            $header[] = 'Districtname';
            $header[] = 'statename';
            $header[] = 'comments';
            
            $logFile = base_path().'/public/logs.csv';
            if(file_exists($logFile))
            {
                unlink($logFile);                
                $f = fopen($logFile, 'w');
                if ($f === false) {
                    die('Error opening the file ' . $logFile);
                }

                fputcsv($f, $header);
                foreach ($logs as $row) {
                    fputcsv($f, $row);
                }

                fclose($f);
            }
        }
    }
}