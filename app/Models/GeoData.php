<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeoData extends Model
{
    protected $table = 'geodata';
    protected $fillable = ['officename'];
}
