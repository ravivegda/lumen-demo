<?php

namespace App\Http\Controllers;
use App\Models\GeoData;
use App\Jobs\ProcessCSV;
use Illuminate\Support\Facades\View;

class TwidController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function readCSV()
    {
        
        dispatch(new ProcessCSV());
        echo "Process added in Queue <br>";
        echo "<a href='logs.csv'>Logs</a>";
    }

    public function list()
    {
        $geodata = GeoData::paginate(10);
        return view('list',compact('geodata'));
    }
}
