<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>India Pincode</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
</head>
<body>

<div class="container mt-5">

    <h1 class="text-center">List</h1>

    <table cellpadding=10 border=1>
            <tr>
                <th>Office Name</th>
                <th>Pincode</th>
                <th>Office Type</th>
                <th>Delivery Status</th>
                <th>Division Name</th>
                <th>Region Name</th>
                <th>Circle Name</th>
                <th>Taluka</th>
                <th>District Name</th>
                <th>State Name</th>
            </tr>
            @if(count($geodata)>0)
                @foreach ($geodata as $data)
                    <tr>
                        <td>{{ $data->officename }}</td>
                        <td>{{ $data->pincode }}</td>
                        <td>{{ $data->officeType }}</td>
                        <td>{{ $data->deliverystatus }}</td>
                        <td>{{ $data->divisionname }}</td>
                        <td>{{ $data->regionname }}</td>
                        <td>{{ $data->circlename }}</td>
                        <td>{{ $data->taluk }}</td>
                        <td>{{ $data->districtname }}</td>
                        <td>{{ $data->statename }}</td>
                    </tr>
                @endforeach 
            @else
                <tr><td colspan="10">No Records Found.</td></tr>
            @endif
                       
    </table>  
    @if(count($geodata)>0) 
        {{ $geodata->links("pagination::bootstrap-4") }}
    @endif
</div>

</body>
</html>

